package mis.pruebas.mongodb.datos;

import mis.pruebas.mongodb.modelo.Producto;
import mis.pruebas.mongodb.modelo.Usuario;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RepositorioProductoPersonalizadoImpl
    implements  RepositorioProductoPersonalizado {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public Producto reemplazarProducto(String id, Producto productoNuevo) {
        // FIXME: OJO! Estoy hay que hacerlo en simetría con crearProducto()!
        // Como lo estamos haciendo ahora, los productos van a quedar con los objetos
        // Usuario adentro del producto en la base de datos, pero en el crearProducto()
        // estamos separando los objetos Usuario a su propia colección.
        // QUEDA COMO EJERCICIO hacer este metodo reemplazarProducto() de modo que separe los
        // usuarios en su propia colección.
        // Tomar como base el crearProducto() RepositorioProductoPersonalizadoImpl.
        final Producto p = this.mongoOperations.findById(id, Producto.class);
        if(p != null) {
            productoNuevo.setId(id);
            this.mongoOperations.save(productoNuevo);
            return productoNuevo;
        }
        return null;
    }

    @Override
    public List<Producto> obtenerProductos() {
        final LookupOperation join = Aggregation.lookup(
                "usuarios",
                "ids_usuarios",
                "_id",
                "usuarios");
        final AggregationResults<Producto> r =
            this.mongoOperations.aggregate(
                Aggregation.newAggregation(join),
                Producto.class,
                Producto.class);
        return r.getMappedResults();
}

    @Override
    public void crearProducto(Producto producto) {
        final List<Usuario> usuarios = producto.getUsuarios();
        usuarios.stream().forEach(u -> {
            final Usuario uu = this.mongoOperations.save(u);
            u.setId(uu.getId());
        });

        producto.setUsuarios(null);
        final Producto pp = this.mongoOperations.save(producto);
        producto.setId(pp.getId());
        producto.setUsuarios(usuarios);

        Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(producto.getId()));

        List<ObjectId> ids_usuarios = usuarios.stream().map(u -> new ObjectId(u.getId())).collect(Collectors.toList());
        Update u = new Update();
        u.set("ids_usuarios", ids_usuarios);

        this.mongoOperations.updateFirst(q, u, Producto.class);
    }
}
