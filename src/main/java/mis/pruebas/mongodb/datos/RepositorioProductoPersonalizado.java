package mis.pruebas.mongodb.datos;

import mis.pruebas.mongodb.modelo.Producto;

import java.util.List;

public interface RepositorioProductoPersonalizado {
    public List<Producto> obtenerProductos();
    public void crearProducto(Producto producto);
    public Producto reemplazarProducto(String id, Producto productoNuevo);
}
